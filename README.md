# vueschool-forum

> This is an interesting, highly interactive school forum built as a side project.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# test app
npm run test

# build for production and view the bundle analyzer report
npm run build --report
```
